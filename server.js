'use strict';

var express = require('express');
var mongo = require('mongodb');
var mongoose = require('mongoose');
const bodyParser = require('body-parser');
const dns = require('dns');
const md5 = require('md5');

var cors = require('cors');

var app = express();

// Basic Configuration 
var port = process.env.PORT || 3000;

/** this project needs a db !! **/ 
mongoose.connect(process.env.MONGO_URI, { useNewUrlParser: true, useUnifiedTopology: true }); 


let Schema = mongoose.Schema;

let shortUrlSchema = Schema({
  original_url: String,
  short_url: String
});
let shortUrl = mongoose.model('shortUrl',shortUrlSchema);

app.use(cors());

/** this project needs to parse POST bodies **/
// you should mount the body-parser here
app.use(bodyParser());

app.use('/public', express.static(process.cwd() + '/public'));

app.get('/', function(req, res){
  res.sendFile(process.cwd() + '/views/index.html');
});

  
// your first API endpoint... 
app.post("/api/shorturl/new", (req, res) => {
  let url = req.body.url;
  let regex = /https?:\/\/(.*)(\/.*)?/
  dns.lookup(url.match(regex)[1],(err) => {
    if(err){
      res.json({"error":"invalid URL"});
    }
    shortUrl.exists({original_url: req.body.url},(err,data) =>{
      if(err){
        console.log(err);
      }
      if(data == false){
        let shorturl = new shortUrl({
          original_url:req.body.url,
          short_url:md5(req.body.url).substring(0,5)
        });
        
        shorturl.save((err,data) => {
          if(err){
            console.log(err);
          }
        });
      }
      res.json({
          original_url:req.body.url,
          shol:md5(req.body.url).substring(0,5)
        });
    });
  });
  
});


app.listen(port, function () {
  console.log('Node.js listening ...');
});